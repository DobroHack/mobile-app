import React, { Component } from "react";
import GenerateForm from "react-native-form-builder";
import { View, Text, Button } from 'native-base';
import { AppRegistry } from 'react-native';
const fields = [
  {
    type: 'text',
    name: 'email',
    required: true,
    label: 'Email'
  },
  {
    type: 'password',
    name: 'password',
    required: true',
    label: 'Пароль'
  },
  {
    type: 'picker',
    name: 'role',
    mode: 'dropdown',
    required: true,
    label: "Кто вы",
    options: ['child', 'candidate'],
    defaultValue: 'child'
  }
]

const styles = {
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
};


export default class Login extends Component {
  login() {
    const user = this.formGenerator.getValues();
    axios.post("https://betwen.online/users.json", {user}).then(data=>{
      console.log(data)
      this.props.onLogin(data)
    }).catch((error)=>{
      console.log(error)
      axios.post("https://betwen.online/users/sign_in.json", {user}).then(data=>{
        console.log(data)
        this.props.onLogin(data)
      }).catch(console.log)
    })
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <View>
          <GenerateForm
            ref={(c) => {
              this.formGenerator = c;
            }}
            fields={fields}
          />
        </View>
        <View style={styles.submitButton}>
          <Button block onPress={() => this.login()}>
            <Text>Login</Text>
          </Button>
        </View>
      </View>
    );
  }
}

AppRegistry.registerComponent('FormGenerator', () => FormGenerator);
