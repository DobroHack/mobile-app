/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button } from "react-native";
import Chat from "./chat";
import Login from './login';
import Chats from './chats';

import { Accelerometer } from "react-native-sensors";
import RNExitApp from "react-native-exit-app";
type Props = {};
export default class App extends Component<Props> {
  state = {
    user: {},
    page: "login", // login chats chat
  };

  onLogin = (user) => {
    this.setState({user, page: 'chats'})
  }

  componentDidMount() {
    new Accelerometer().then(observable => {
      observable.subscribe(({ x, y, z }) => {
        const pain = Math.abs(Math.sqrt(x * x + y * y + z * z) - 9.8) > 4;
        if (pain) RNExitApp.exitApp();
      });
    })
    .catch(error => {
      console.log("The sensor is not available");
    });
  }

  selectChat = (chat) => {
    this.setState({
      page: 'chat',
      chat: chat,
    })
  }
  render() {
    switch(this.state.page) {
      case "login":
        return <Login onLogin={this.onLogin}/>
      case: "chats":
        return <Chats user={this.state.user} onSelect={this.selectChat} />
      case: "chat":
        return <Chat user={this.state.user} chat={this.state.chat} />
      default:
        return <Login onLogin={this.onLogin} />
    }
  }
}
