import React, { Component } from "react";
import { View, Text, Button } from "native-base";
import axios from "axios";

export default class Chats extends Component {
  state = {
    chats: []
  };
  componentDidMount() {
    const { email, authentication_token } = this.props.user;
    axios
      .get(
        `https://between.online/api/v1/chats.json?user_email=${email}&user_token=${authentication_token}`
      )
      .then(chats => {
        this.setState({ chats });
      })
      .catch(error => {
        this.setState({ error });
      });
  }
  render() {
    if (this.state.error)
      return (
        <View>
          <Text>{JSON.stringify(this.state.error)}</Text>
        </View>
      );
    return (
      <View>
        {this.chats.map(chat => (
          <Button onPress={this.props.onSelect(chat)} key={chat.id}>
            <Text>{chat.title}</Text>
          </Button>
        ))}
      </View>
    );
  }
}
