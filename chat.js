import React, { Component } from "react";

import { View } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";

import ActionCable from "react-native-actioncable";

type Props = {};

export default class Chat extends Component<Props> {
  state = {
    messages: []
  };
  toGifted(message) {
    return {
      _id: message.id,
      text: message.content,
      user: {
        _id: 2,
        name: "React Native",
        avatar: "https://placeimg.com/140/140/any"
      }
    };
  }
  fromGifted(message) {
    return {
      id: message._id,
      content: message.text
    };
  }
  componentWillMount() {
    //fetch("http://localhost:4319/kaka");
    //fetch("http://10.0.2.2:4319/akak");
    //fetch("https://yandex.ru");
    const token = this.props.user.authentication_token;
    const email = this.props.user.email;
    axios
      .get(
        `https://between.online/api/v1/chats/${
          this.props.chat.id
        }/messages.json?user_email=${email}&user_token=${authentication_token}`
      )
      .then(messages => {
        this.setState({ messages });
      })
      .catch(error => {
        this.setState({ messages });
      });
    const cable = ActionCable.createConsumer(
      `wss://between.online/cable?user_email=${email}&user_token=${token}`
    );
    const that = this;
    cable.subscriptions.create(
      { channel: "ChatChannel", id: 1 },
      {
        received(data) {
          console.log(data.message);
          that.setState({
            messages: [
              ...that.state.messages,
              that.toGifted(JSON.parse(data.message))
            ]
          });
        }
      }
    );
  }

  onSend(messages = []) {
    axios
      .post(
        `https://between.online/chats/${this.props.chat.id}/messages.json?`,
        { message: this.fromGifted(messages[0]) }
      )
      .then(message => {})
      .catch(error => {
        this.setState({ error });
      });
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));
  }

  render() {
    if (this.state.error) {
      return (
        <View>
          <Text>{JSON.stringify(this.state.error)}</Text>
        </View>
      );
    }
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: this.props.user.id
        }}
      />
    );
  }
}
